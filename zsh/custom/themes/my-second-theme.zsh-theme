# Personal theme on zsh

#PROMPT="%(?:%{$fg_bold[cyan]%}.../:%{$fg_bold[red]%}.../)"
PROMPT=""
if [[ $USER == "root" ]]; then
  PROMPT+="%{$fg_bold[red]%}# "
fi

#PROMPT+="%(?:%{$fg_bold[cyan]%}:%{$fg_bold[red]%})%c %{$fg_bold[blue]%}[ "
#PROMPT+="%(?:%{$fg_bold[cyan]%}:%{$fg_bold[red]%})%~ %{$fg_bold[blue]%}[ "
#local ret_status="%{$fg_bold[cyan]%}[%(?:%{$fg_bold[green]%}:%{$fg_bold[red]%})%?%{$fg_bold[cyan]%}]"
local ret_status="%(?:%{$fg_bold[green]%}:%{$fg_bold[red]%})%?%{$reset_color%}"

local init_prompt="%(?:%{$fg_bold[green]%}:%{$fg_bold[red]%})$%{$reset_color%}"
local finish_prompt="%(?:%{$fg_bold[green]%}:%{$fg_bold[red]%})]%{$reset_color%}"
#local prompt_status="%{$fg_bold[cyan]%}[%{$ret_status%}%{$fg_bold[cyan]%}]"

PROMPT+="%{$fg_bold[cyan]%}%c %{$init_prompt%}  "

#RPROMPT='$(git_prompt_info) %{$fg_bold[cyan]%}%D{%L:%M}%f '
RPROMPT='$(git_prompt_info) [%?]'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}  "
ZSH_THEME_GIT_PROMPT_SUFFIX="%f"

ZSH_THEME_GIT_PROMPT_DIRTY=" %F{red}*%f"
ZSH_THEME_GIT_PROMPT_CLEAN=""

ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%} +"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[yellow]%} ~"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%} -"
#ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%} ➦"
#ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[magenta]%} ✂"
#ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[grey]%} ✱"
