# Personal theme on zsh

#PROMPT="%(?:%{$fg_bold[cyan]%}.../:%{$fg_bold[red]%}.../)"
PROMPT=""
if [[ $USER == "root" ]]; then
  PROMPT+="%{$fg_bold[red]%}# "
fi

PROMPT+="%(?:%{$fg_bold[cyan]%}:%{$fg_bold[red]%})%c %{$fg_bold[blue]%}[ "
RPROMPT='$(git_prompt_info) %{$fg_bold[blue]%}] %{$fg_bold[cyan]%}%D{%L:%M}%f'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%} on "
ZSH_THEME_GIT_PROMPT_SUFFIX="%f"

ZSH_THEME_GIT_PROMPT_DIRTY=" %F{red}*%f"
ZSH_THEME_GIT_PROMPT_CLEAN=""

ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%} +"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[yellow]%} ~"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%} -"
#ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%} ➦"
#ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[magenta]%} ✂"
#ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[grey]%} ✱"
