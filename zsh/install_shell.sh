#!/bin/bash
dir="$(pwd)"

# Instalar dependencias de zsh
echo "Instalando zsh y dependencias"
# sudo pacman -S git lsd bat
# sudo apt install zsh git lsd bat
# sudo yum install zsh git lsd bat
chsh -s $(which zsh)

echo "Instalando ohmyzsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo "Instalando plugins de ohmyzsh"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

echo "Agregando tema"
curl https://gitlab.com/afmartinr/configs/-/raw/main/zsh/my-theme.zsh-theme\?ref_type\=heads > ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/my-theme.zsh-theme

echo "Actualizando .zshrc"
curl https://gitlab.com/afmartinr/configs/-/raw/main/zsh/zshrc?ref_type=heads > ~/.zshrc

echo 'Instalando fuente Hack de https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/Hack.zip'
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/Hack.zip
unzip Hack.zip
mkdir $HOME/.fonts
mv *.ttf $HOME/.fonts
rm *.md Hack.zip

#ln -sf "$dir/zsh/zshrc" "$HOME/.zshrc"
